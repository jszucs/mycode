#!/usr/bin/env python

import json
import yaml 
import csv
import pandas as pd

with open('text.csv') as f:
    txt = csv.DictReader(f)
    data = [d for d in txt]
    print(json.dumps(data))
    print(yaml.dump(data))
