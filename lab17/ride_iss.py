#!/usr/bin/python3
"""Alta3 Research - astros on ISS"""

import urllib.request
import json
import requests

MAJORTOM = "http://api.open-notify.org/astros.json"

def main():
    """reading json from api"""
    # call the api
    groundctrl_urllib = urllib.request.urlopen(MAJORTOM)
    groundctrl = requests.get(MAJORTOM)

    # strip off the attachment (JSON) and read it
    # the problem here, is that it will read out as a string
    helmet = groundctrl_urllib.read()
    

    # show that at this point, our data is str
    # we want to convert this to list / dict
    #print(helmet)

    #helmetson = json.loads(helmet.decode("utf-8"))
    helmetson = groundctrl.json()

    # this should say bytes
    #print('Should be bytes:')
    #print(type(helmet))

    # this should say dict
    #print('Should be dict:')
    #print(type(helmetson))

    print(helmetson["number"])

    # this returns a LIST of the people on this ISS
    #print(helmetson["people"])

    # list the FIRST astro in the list
    #print(helmetson["people"][0])

    # list the SECOND astro in the list
    #print(helmetson["people"][1])

    # list the LAST astro in the list
    #print(helmetson["people"][-1])

    # display every item in a list
    '''
    for astro in helmetson["people"]:
        # display what astro is
        print(astro)
    # display every item in a list
    for astro in helmetson["people"]:
        # display ONLY the name value associated with astro
        print(astro["name"])
   '''
    print('----------CHALLENGE 01-------------')
    for each in helmetson['people']:
        print(each['name']+' on the '+each['craft'])

if __name__ == "__main__":
    main()

