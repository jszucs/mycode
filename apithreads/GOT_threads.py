#!/usr/bin/python3
"""API requests with threads | rzfeeser@alta3.com"""

# standard library
from concurrent.futures import ThreadPoolExecutor, as_completed
from time import time

# python3 -m pip install requests
import requests


url = 'https://anapioficeandfire.com/api/characters/'

def download_file(url):
    html = requests.get(url, stream=True)
    return html.json()

start = time()

processes_dict = {} 
processes = []
# we want to be careful with the number of workers
# if you are making thousands of requests, does your target have limiting engaged?
# beware you don't overload internal or external services; 5 to 10 is fine for most scripts
with ThreadPoolExecutor(max_workers=5) as executor:
    for index in range(1, 101):
        new_url = url+str(index)
        processes.append(executor.submit(download_file, new_url))   # add a new task to the threadpool and store in processes list
        processes_dict[index] = executor.submit(download_file, new_url)
counter = 1
for task in as_completed(processes):    # yields the items in processes as they complete (it finished or was canceled)
    print(counter,': ',task.result()['name'])
    counter+=1

print(as_completed(processes_dict))

# display the total run time
print(f'Time taken: {time() - start}')
