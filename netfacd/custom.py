#!/usr/bin/env python3

import netifaces
import sys

def get_ip(ifname):
    try:
        ip_addr = netifaces.ifaddresses(ifname)[netifaces.AF_INET][0]['addr']
    except:
        ip_addr = 'Not Found'
    return ip_addr

def get_mac(ifname):
    try:
        mac = netifaces.ifaddresses(ifname)[netifaces.AF_LINK][0]['addr']
    except:
        mac = 'Not Found'
    return mac 

print('Interfaces detected: ')
for i in netifaces.interfaces():
    print(i)
iface = input('Input Interface Name: ')

#test =  netifaces.ifaddresses(iface)
#print(test)
ip_address = get_ip(iface)
mac_address = get_mac(iface)
print('IP address for',iface,': ',ip_address)
print('MAC address for',iface,': ',mac_address)



