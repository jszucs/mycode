from time import sleep
from flask import Flask

app = Flask(__name__)


@app.route("/<fw>/<bang_time>")
def launch(fw, bang_time):
    sleep(int(bang_time))
    return f"""

        .
      .' |
    .'   |
    /`-._'
   /   /
  /   /
 /   /
(`-./
 )

{fw} launched!
"""


if __name__ == "__main__":
    app.run(port=1776)
