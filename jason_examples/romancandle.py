import time
import requests


fireworks = [
    ("Roman Candle", 1),
    ("Report", 3),
    ("Peony", 4),
    ("Palm Tree", 3),
    ("Pistil", 5),
]


def launch_firework(firework):
    print(f"launching a {firework[0]}!")
    return requests.get(f"http://0.0.0.0:1776/{firework[0]}/{firework[1]}")


start = time.time()

processes = []

print("Starting the fireworks show!")
for fw in fireworks:
    print(launch_firework(fw).text)


# display the total run time
print(f'Time taken: {time.time() - start}')
